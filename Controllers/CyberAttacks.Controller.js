const createError = require('http-errors');
const mongoose = require('mongoose');

const CyberAttacks = require('../Models/CyberAttacks.model');

module.exports = {
  getAllCyberAttacks: async (req, res, next) => {
    try {
      const results = await CyberAttacks.find();
      res.status(200).send(results);
    } catch (error) {
      console.log(error.message);
      next(error);
    }
  },

  createNewCyberAttack: async (req, res, next) => {
    try {
      const cyberAttacks = new CyberAttacks(req.body);
      const result = await cyberAttacks.save();
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error.name === 'ValidationError') {
        next(createError(422, error.message));
        return;
      }
      next(error);
    }
  },

  findCyberAttackById: async (req, res, next) => {
    const id = req.params.id;
    try {
      const cyberAttack = await CyberAttacks.findOne({ _id: id });
      if (!cyberAttack) {
        throw createError(404, 'CyberAttack does not exist.');
      }
      res.status(200).send(cyberAttack);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid CyberAttack id'));
        return;
      }
      next(error);
    }
  },

  updateACyberAttack: async (req, res, next) => {
    try {
      const id = req.params.id;
      const updates = req.body;
      const options = { new: true };

      const result = await CyberAttacks.findByIdAndUpdate(id, updates, options);
      if (!result) {
        throw createError(404, 'CyberAttack does not exist');
      }
      res.status(200).send(result);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        return next(createError(400, 'Invalid CyberAttack Id'));
      }

      next(error);
    }
  },

  deleteACyberAttack: async (req, res, next) => {
    const id = req.params.id;
    try {
      const result = await CyberAttacks.findByIdAndDelete(id);
      if (!result) {
        throw createError(404, 'CyberAttack does not exist.');
      }
      const response = {
        message: "Todo successfully deleted",
        id: req.params.id
      };
      return res.status(200).send(response);
    } catch (error) {
      console.log(error.message);
      if (error instanceof mongoose.CastError) {
        next(createError(400, 'Invalid CyberAttack id'));
        return;
      }
      next(error);
    }
  }
};

const express = require('express');
const router = express.Router();

const CyberProtectionController = require('../Controllers/CyberProtection.Controller');

//Get a list of all CyberProtection
router.get('/', CyberProtectionController.getAllCyberProtection);

//Create a new CyberProtection
router.post('/', CyberProtectionController.createNewCyberProtection);

//Get a CyberProtection by id
router.get('/:id', CyberProtectionController.findCyberProtectionById);

//Update a CyberProtection by id
router.patch('/:id', CyberProtectionController.updateACyberProtection);

//Delete a CyberProtection by id
router.delete('/:id', CyberProtectionController.deleteACyberProtection);

module.exports = router;

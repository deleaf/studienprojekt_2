const express = require('express');
const router = express.Router();

const TrainingMaterialController = require('../Controllers/TrainingMaterial.Controller');

//Get a list of all TrainingMaterial
router.get('/', TrainingMaterialController.getAllTrainingMaterial);

//Create a new TrainingMaterial
router.post('/', TrainingMaterialController.createNewTrainingMaterial);

//Get a TrainingMaterial by id
router.get('/:id', TrainingMaterialController.findTrainingMaterialById);

//Update a TrainingMaterial by id
router.patch('/:id', TrainingMaterialController.updateATrainingMaterial);

//Delete a TrainingMaterial by id
router.delete('/:id', TrainingMaterialController.deleteATrainingMaterial);

module.exports = router;

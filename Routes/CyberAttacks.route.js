const express = require('express');
const router = express.Router();

const CyberAttacksController = require('../Controllers/CyberAttacks.Controller');

//Get a list of all CyberAttacks
router.get('/', CyberAttacksController.getAllCyberAttacks);

//Create a new CyberAttack
router.post('/', CyberAttacksController.createNewCyberAttack);

//Get a CyberAttack by id
router.get('/:id', CyberAttacksController.findCyberAttackById);

//Update a CyberAttack by id
router.patch('/:id', CyberAttacksController.updateACyberAttack);

//Delete a CyberAttack by id
router.delete('/:id', CyberAttacksController.deleteACyberAttack);

module.exports = router;

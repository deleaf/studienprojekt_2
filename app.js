const express = require('express');
const createError = require('http-errors');
const dotenv = require('dotenv').config();
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Initialize DB
require('./initDB')();

const CyberAttacksRoute = require('./Routes/CyberAttacks.route');
app.use('/cyberAttacks', CyberAttacksRoute);

const CyberProtectionRoute = require('./Routes/CyberProtection.route');
app.use('/cyberProtections', CyberProtectionRoute);

const TrainingMaterialRoute = require('./Routes/TrainingMaterial.route');
app.use('/trainingMaterials', TrainingMaterialRoute);

const ContactPersonRoute = require('./Routes/ContactPerson.route');
app.use('/contactPersons', ContactPersonRoute);

//404 handler and pass to error handler
app.use((req, res, next) => {
  next(createError(404, 'Site not found'));
});

//Error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    error: {
      status: err.status || 500,
      message: err.message
    }
  });
});
const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log('Server started on port ' + PORT + '...');
});
